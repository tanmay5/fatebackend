# Generated by Django 3.0 on 2019-12-07 03:59

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Jobs',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('id_job', models.CharField(max_length=25)),
                ('role', models.CharField(max_length=100)),
                ('percentage', models.IntegerField()),
            ],
        ),
    ]
