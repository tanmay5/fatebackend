from django.urls import path, include
from jobs.api import SaveDefault

urlpatterns = [
    path('', SaveDefault.as_view(), name="Send model request"),
]