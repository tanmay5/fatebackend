from django.contrib import admin
from jobs.models import Jobs
# Register your models here.

class JobsDemo(admin.ModelAdmin):
    list_display = ['id', 'role', 'percentage']

admin.site.register(Jobs, JobsDemo)