from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from jobs.models import Jobs
from jobs.serializers import JobsSerializer


class SaveDefault(APIView):
    def get(self, request):

        job1 = Jobs.objects.get_or_create(id_job="100000000000000000000001", role="job 1", percentage = 2)
        job2 = Jobs.objects.get_or_create(id_job="100000000000000000000002", role="job 2", percentage = 12)
        job3 = Jobs.objects.get_or_create(id_job="100000000000000000000003", role="job 3", percentage = 42)
        job4 = Jobs.objects.get_or_create(id_job="100000000000000000000004", role="job 4", percentage = 22)
        job5 = Jobs.objects.get_or_create(id_job="100000000000000000000005", role="job 5", percentage = 52)

        job1 = Jobs.objects.get(id_job="100000000000000000000001")
        job2 = Jobs.objects.get(id_job="100000000000000000000002")
        job3 = Jobs.objects.get(id_job="100000000000000000000003")
        job4 = Jobs.objects.get(id_job="100000000000000000000004")
        job5 = Jobs.objects.get(id_job="100000000000000000000005")


        return Response({"jobs": [JobsSerializer(job1).data, JobsSerializer(job2).data, JobsSerializer(job3).data, JobsSerializer(job4).data, JobsSerializer(job5).data]})

        