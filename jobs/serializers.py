from rest_framework import serializers
from jobs.models import Jobs

class JobsSerializer(serializers.Serializer):
    id_job = serializers.CharField(max_length=25)
    role = serializers.CharField(max_length=100)
    percentage = serializers.IntegerField()