from django.db import models

# Create your models here.
class Jobs(models.Model):
    id_job = models.CharField(max_length=25)
    role = models.CharField(max_length=100)
    percentage = models.IntegerField()

    def __str__(self):
        return self.id_job