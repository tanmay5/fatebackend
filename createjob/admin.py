from django.contrib import admin
from createjob.models import Jobs
# Register your models here.

class JobsDemo(admin.ModelAdmin):
    list_display = ['id', 'role', 'percentage']

admin.site.register(Jobs, JobsDemo)