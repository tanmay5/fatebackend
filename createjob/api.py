from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from createjob.models import Jobs
from createjob.serializers import JobsSerializer


class SaveDefault(APIView):
    def post(self, request):

        if Jobs.objects.filter(id_job="100000000000000000000006").exists():
            Jobs.objects.get(id_job="100000000000000000000006").delete()
        job = Jobs.objects.get_or_create(id_job="100000000000000000000006", role="job 6", percentage = 0)
        
        job = Jobs.objects.get(id_job="100000000000000000000006")
        


        return Response({"jobs": JobsSerializer(job).data})

    def put(self, request):

        job = Jobs.objects.get(id_job="100000000000000000000006")
        if job.percentage != 100:

            job.percentage = job.percentage + 1
            job.save()

        return Response({"jobs": JobsSerializer(job).data})

    def get(self, request):

        if Jobs.objects.filter(id_job="100000000000000000000006").exists():
            job = Jobs.objects.get(id_job="100000000000000000000006")
            return Response({"jobs": JobsSerializer(job).data})
        
        return Response({"jobs": {"id_job":"0"}})

    def delete(self, request):

        if Jobs.objects.filter(id_job="100000000000000000000006").exists():
            Jobs.objects.get(id_job="100000000000000000000006").delete()
            return Response({"job": "No"})

        return Response({"job": "No"})
